/*
 * @Author: ���� 371824572@qq.com
 * @Date: 2023-12-14 08:55:10
 * @LastEditors: ���� 371824572@qq.com
 * @LastEditTime: 2023-12-26 14:03:12
 * @FilePath: \blog\vite.config.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// Plugins
import vue from "@vitejs/plugin-vue";
import vuetify, { transformAssetUrls } from "vite-plugin-vuetify";

// Utilities
import { defineConfig } from "vite";
import { fileURLToPath, URL } from "node:url";

// 让项目支持require导入模块 import vitePluginRequire from 'vite-plugin-require'
import requireTransform from "vite-plugin-require-transform";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: { transformAssetUrls },
    }),
    // https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
    vuetify({
      autoImport: true,
    }),
    requireTransform({
      fileRegex: /.ts$|.tsx$|.vue$/,
    }),
  ],
  define: { "process.env": {} },
  base:'./',
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
    extensions: [".js", ".json", ".jsx", ".mjs", ".ts", ".tsx", ".vue"],
  },
  css: {
    // css预处理器
    preprocessorOptions: {
      scss: {
        charset: false,
      },
    },
  },
  server: {
    port: 3000,
    host: "0.0.0.0",
    hmr: true,
    proxy: {
      "/api": {
        target: "http://localhost:53785",
        ws: true,
        changeOrigin: true,
        secure: false, //解决target使用https出错问题
      },
    },
  },
});
